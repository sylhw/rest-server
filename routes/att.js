const express = require('express');
const { body, header } = require('express-validator');

const attController = require('../controller/att');


const router = express.Router();




router.post(
  '/updateRecord',
  header(),
  body(),
  attController.postData
);

router.post(
  '/attData',
  header(),
  body(),
  attController.postData1
);



module.exports = router;
