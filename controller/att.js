const fs = require('fs');
const path = require('path');

const { validationResult } = require('express-validator');






exports.postData = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Validation failed, entered data is incorrect.');
    error.statusCode = 422;
    throw error;
  }
  const mqttData = req.body.mqttdata;

  try {

    const data=req.body;
    console.log("Endpoint Name = api/record");
    const apiData=JSON.stringify(data,undefined,2);

    console.log("apiData = "+apiData);

    console.log("Header data");
    console.log(req.headers)
    const headers=req.headers;

    const headersData=JSON.stringify(headers,undefined,2);

    var pktType=parseInt(data.data.packetType);
    console.log(typeof pktType);
    console.log("pktType="+pktType);
    var temperatureScanType=parseInt(data.data.temperatureScanType);
    console.log("temperatureScanType="+temperatureScanType);



    var clientId=req.get('clientId')
    console.log("clientId = "+clientId)

    var clientsecretkey=req.get('clientsecretkey')
    console.log("clientsecretkey = "+clientsecretkey)


    res.status(201).json({
      message: 'Data received successfully!',
     
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.postData1 = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Validation failed, entered data is incorrect.');
    error.statusCode = 422;
    throw error;
  }
  const mqttData = req.body.mqttdata;

  try {

    const data=req.body;
    console.log("API Name = att/attData");
    const attDat=JSON.stringify(data);
    console.log("attadata = "+attDat);


    console.log(req.headers)

    var clientId=req.get('clientId')
    console.log("clientId = "+clientId)

    var clientsecretkey=req.get('clientsecretkey:')
    console.log("clientsecretkey = "+clientsecretkey)

    res.status(201).json({
      message: 'Data received successfully!',
     
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};



